// auth.js
import Token from "@/token-usage.js";
import { auth } from '@/firebase-config.js'
import { GoogleAuthProvider, signInWithPopup, signInWithCredential, signOut } from 'firebase/auth'

export default {
    namespaced: true,
    state: {
        user: null,

        loading: false,
        error: null,
    },
    getters: {
        getUser: (state) => state.user,
        userIsAuthenticated: (state) => state.user !== null,
    },
    mutations: {
        setUser(state, user) {
            state.user = user
        },
        setLoading(state, loading) {
            state.loading = loading
        },
        setError(state, error) {
            state.error = error
        },
    },

  actions: {
    async saveLoginUserData({ commit,dispatch }, loginResult) {
      const user = loginResult?.user;
      commit('setUser', user);

      
      const tokenExpirationTime = new Date().getTime() + 30 * 60 * 1000;
      const token = user?.uid; 

      
      Token.setAccessTokenCookie(token, tokenExpirationTime);
      Token.setUserDataCookie(user);
      let credential = GoogleAuthProvider.credentialFromResult(loginResult)

            localStorage.setItem('authCredential', JSON.stringify(credential))

            dispatch('users/loadUserPermissions', user.uid, { root: true })
    },

    loginWithGoogle({ commit, dispatch }) {
      const provider = new GoogleAuthProvider()
      signInWithPopup(auth, provider)
          .then((loginResult) => {
              dispatch('saveLoginUserData', loginResult)
          })
          .catch((error) => {
              commit('setError', error)
          })
  },
  async loginWithCredential({ commit, dispatch }) {
    const credential = localStorage.getItem('authCredential');
  
    if (credential) {
      try {
        const parsedCredential = JSON.parse(credential);
        const authCredential = GoogleAuthProvider.credential(parsedCredential.idToken);
        const loginResult = await signInWithCredential(auth, authCredential);
        dispatch('saveLoginUserData', loginResult);
        return loginResult;
      } catch (error) {
        console.error(error);
        commit('setError', error);
        return false;
      }
    } else {
      return false;
    }
  },
  logout({ commit, dispatch }) {
      signOut(auth)
          .then(() => {
              localStorage.removeItem('authCredential')
              commit('setUser', null)
              dispatch('users/clearPermissions', null, { root: true })
          })
          .catch((error) => {
              commit('setError', error)
          })
  },

  },
};
