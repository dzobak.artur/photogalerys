import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore/lite'
import { getAuth } from 'firebase/auth'

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDtVF0faUMS6dHFDoP2jEkPYVqSNDYnjkk",
    authDomain: "project-187d8.firebaseapp.com",
    projectId: "project-187d8",
    storageBucket: "project-187d8.appspot.com",
    messagingSenderId: "163404063303",
    appId: "1:163404063303:web:fb13df48543e3cad534762"
  };

  const app = initializeApp(firebaseConfig)
  export const auth = getAuth(app)
  const db = getFirestore(app)
  export default db
  

  