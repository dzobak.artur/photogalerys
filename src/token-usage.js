class Token {
  static setAccessTokenCookie(token, expirationTime) {
    const cookieString = `access_token=${token}; expires=${new Date(
      expirationTime
    ).toUTCString()}; path=/;`;

    if (window.location.protocol === "https:") {
      document.cookie = `${cookieString} Secure; SameSite=None;`; 
    } else {
      document.cookie = cookieString; 
    }
  }

  static getAccessTokenFromCookie() {
    const name = "access_token=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const cookieArray = decodedCookie.split(";");

    for (let i = 0; i < cookieArray.length; i++) {
      let cookie = cookieArray[i].trim();
      if (cookie.indexOf(name) === 0) {
        return cookie.substring(name.length, cookie.length);
      }
    }

    return null;
  }
  
  static removeAccessTokenCookie() {
    const pastDate = new Date(1970, 0, 1).toUTCString(); 

    if (window.location.protocol === "https:") {
      document.cookie = `access_token=; expires=${pastDate}; path=/; Secure; SameSite=None;`; 
    } else {
      document.cookie = `access_token=; expires=${pastDate}; path=/;`; 
    }
  }

  static setUserDataCookie(user) {
    const userData = {
      displayName: user.displayName,
      photoURL: user.photoURL,
      email: user.email, 
    };
  
    const userDataString = `user_data=${JSON.stringify(userData)}; path=/;`;
  
    if (window.location.protocol === "https:") {
      document.cookie = `${userDataString} Secure; SameSite=None;`;
    } else {
      document.cookie = userDataString;
    }
  }

  static getUserDataFromCookie() {
    const name = "user_data=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const cookieArray = decodedCookie.split(";");

    for (let i = 0; i < cookieArray.length; i++) {
      let cookie = cookieArray[i].trim();
      if (cookie.indexOf(name) === 0) {
        return JSON.parse(cookie.substring(name.length));
      }
    }

    return null;
  }

  static removeUserDataCookie() {
    const pastDate = new Date(1970, 0, 1).toUTCString(); 

    if (window.location.protocol === "https:") {
      document.cookie = `user_data=; expires=${pastDate}; path=/; Secure; SameSite=None;`; 
    } else {
      document.cookie = `user_data=; expires=${pastDate}; path=/;`; 
    }
  }
}

export default Token;
